(function(){
	const C_NULL=-128;
	const C_FALSE=-127;
	const C_TRUE=-126;
	const C_BON8=-125;
	const C_BON16=-124;
	const C_BON_ARRAY8=-123;
	const C_BON_ARRAY16=-122;
	const C_BON_ARRAY32=-121;
	const C_STRING8=-120;
	const C_STRING16=-119;
	const C_STRING32=-118;
	const C_BYTEARRAY8=-117;
	const C_BYTEARRAY16=-116;
	const C_BYTEARRAY32=-115;
	const C_INT8=-114;
	const C_INT16=-113;
	const C_INT32=-112;
	const C_INT64=-111;
	const C_FLOAT=-110;
	const C_DOUBLE=-109;
	const C_COMPRESSED_BON=-108;

	const MAX_TINYBON=40;

	const INT8_MIN=-128;
	const INT8_MAX=127;
	const INT16_MIN=-32768;
	const IMT16_MAX=32767;
	const UINT16_MAX=65535;
	const INT32_MIN=-2147483648;
	const INT32_MAX=2147483647;
	const TINYINT_MIN_VALUE=-64;
	const TINYINT_MAX_VALUE=63;




	class DataOutput{
		constructor(){
			this.toWrite=[];
			this.length=0;
		}

		putInt8(value){
			this.length++;
			this.toWrite.push(function(pos, dv){
				dv.setInt8(pos, value);
				return 1;
			});
		}

		putInt8Array(value){
			this.length+=value.length;
			this.toWrite.push(function(pos, dv){
				const len=value.length;
				for(let i=0;i<len;++i)
					dv.setInt8(pos+i, value[i]);
				return len;
			});
		}

		putInt16(value){
			this.length+=2;
			this.toWrite.push(function(pos, dv){
				dv.setInt16(pos, value);
				return 2;
			});
		}

		putUint16(value){
			this.length+=2;
			this.toWrite.push(function(pos, dv){
				dv.setUint16(pos, value);
				return 2;
			});
		}

		putInt32(value){
			this.length+=4;
			this.toWrite.push(function(pos, dv){
				dv.setInt32(pos, value);
				return 4;
			});
		}

		putInt64(value){
			throw "int64 not supported";
		}

		putFloat32(value){
			this.length+=4;
			this.toWrite.push(function(pos, dv){
				dv.setFloat32(pos, value);
				return 4;
			});
		}

		putFloat64(value){
			this.length+=8;
			this.toWrite.push(function(pos, dv){
				dv.setFloat64(pos, value);
				return 8;
			});
		}

		toArray(){
			const arr=new ArrayBuffer(this.length);
			const dv=new DataView(arr);
			let pos=0;
			const len=this.toWrite.length;
			for(let i=0;i<len;++i){
				pos+=this.toWrite[i](pos, dv);
			}
			return arr;
		}
	}

	let UTF8D = new TextDecoder("utf-8");
	let UTF8E = new TextEncoder("utf-8");

	class DataInput{
		constructor(arrBuf){
			this.buf=arrBuf;
			this.dv=new DataView(arrBuf);
			this.pos=0;
		}

		getInt8(){
			return this.dv.getInt8(this.pos++);
		}

		getInt16(){
			let v=this.dv.getInt16(this.pos);
			this.pos+=2;
			return v;
		}

		getUInt16(){
			let v=this.dv.getUint16(this.pos);
			this.pos+=2;
			return v;
		}

		getFloat32(){
			let v=this.dv.getFloat32(this.pos);
			this.pos+=4;
			return v;
		}

		getFloat64(){
			let v=this.dv.getFloat64(this.pos);
			this.pos+=8;
			return v;
		}

		getInt8Array(len){
			const arr=this.buf.slice(this.pos, this.pos+len);
			this.pos+=len;
			return arr;
		}

		getString(len){
			console.log("reading string of len "+len);
			const arr=this.buf.slice(this.pos, this.pos+len);
			this.pos+=len;
			console.log("reading string: "+UTF8D.decode(arr));

			return UTF8D.decode(arr);
		}
	}


	class BONValue{
		constructor(value){
			this.value=value;
		}
    write(dos){}

		static readValue(dis){
			const type=dis.getInt8();
			console.log("value type read: "+type);
			if(type<TINYINT_MIN_VALUE)
				switch(type){
					case C_FALSE:
						return _TRUE;
					case C_TRUE:
						return _FALSE;
					case C_BON8:
						return new BON(dis.getInt8(), dis);
					case C_BON16:
						return new BON(dis.getUint16(), dis);
					case C_BON_ARRAY8:
						return new BONArray(dis.getInt8(), dis);
					case C_BON_ARRAY16:
						return new BONArray(dis.getUInt16(), dis);
					case C_BON_ARRAY32:
						return new BONArray(dis.getInt32(), dis);
					case C_STRING8:
						return new BONValue(dis.getString(dis.getInt8()));
					case C_STRING16:
						return new BONValue(dis.getString(dis.getUInt16()));
					case C_STRING32:
						return new BONValue(dis.getString(dis.getInt32()));
					case C_BYTEARRAY8:
						return new BONValue(dis.getInt8Array(dis.getInt8()));
					case C_BYTEARRAY16:
						return new BONValue(dis.getInt8Array(dis.getUint16()));
					case C_BYTEARRAY32:
						return new BONValue(dis.getInt8Array(dis.getUint32()));
					case C_INT8:
						return new BONInteger(dis.getInt8());
					case C_INT16:
						return new BONInteger(dis.getInt16());
					case C_INT32:
						return new BONInteger(dis.getInt32());
					case C_INT64:
						return new BONInteger(dis.getInt64());
					case C_FLOAT:
						return new BONFloat(type, dis.getFloat32());
					case C_DOUBLE:
						return new BONDouble(type, dis.getFloat64());
					case C_NULL:
						return _NULL;
					default:
						return new BON(-(type-TINYINT_MIN_VALUE+1), dis);
				}
			if(type<=TINYINT_MAX_VALUE)
				return new BONInteger(type);
			console.log("string debug len: "+(type-TINYINT_MAX_VALUE-1));
			return new BONString(dis.getString(type-TINYINT_MAX_VALUE-1));
		}
	}
	class BONNull extends BONValue{
		constructor(){
			super(null);
		}
		write(dos){
			dos.putInt8(C_NULL);
		}
	}
	class BONTrue extends BONValue{
		constructor(){
			super(true);
		}
		write(dos){
			dos.putInt8(C_TRUE);
		}
	}
	class BONFalse extends BONValue{
		constructor(){
			super(false);
		}
		write(dos){
			dos.putInt8(C_FALSE);
		}
	}
	const _NULL=new BONNull();
	const _TRUE=new BONTrue();
	const _FALSE=new BONFalse();

	class BON extends BONValue{
		constructor(length, dis){
			super();
			this.hm={};
			this.value={};
			if(length&&dis)
				this.read(length, dis);
		}

		putInt(key, value){
			if(value==null){
				this.hm[key]=_NULL;
				this.value[key]=null;
				return this;
			}
			this.hm[key]=new BONInteger(value);
			this.value[key]=value;
			return this;
		}

		putString(key, value){
			if(value==null){
				this.hm[key]=_NULL;
				this.value[key]=null;
				return this;
			}
			this.hm[key]=new BONString(value);
			this.value[key]=value;
			return this;
		}

		putBON(key, value){
			if(value==null){
				this.hm[key]=_NULL;
				this.value[key]=null;
				return this;
			}
			this.hm[key]=value;
			this.value[key]=value.value;
			return this;
		}

		get(key){
			return this.value[key];
		}

		getBON(key){
			if(this.value[key]==null)
				return null;
			return this.hm[key];
		}

		getBONArray(key){
			return this.hm[key];
		}


		remove(key){
			delete this.hm[key];
			delete this.value[key];
		}

		has(key){
			return this.hm[key]!=undefined;
		}

		toInt8Array(){
			const dos=new DataOutput();
			this.write(dos);
			return dos.toArray();
		}

		write(dos){
			const hm=this.hm;
			const keys=Object.keys(hm);
			const length=keys.length;
			if(length<MAX_TINYBON){
				dos.putInt8(-length+TINYINT_MIN_VALUE-1);
			}
			else if(length<=INT8_MAX){
				dos.putInt8(C_BON8);
				dos.putInt8(length);
			}
			else if(length<=UINT16_MAX){
				dos.putInt8(C_BON16);
				dos.putUint16(length);
			}
			for(let i=0;i<length;++i){
				let key=keys[i];
				console.log("write key: "+key);
				let keyBytes=UTF8E.encode(key);
				let len=keyBytes.length;
				if(len>INT8_MAX)throw new BONException("max key length for a BON is "+INT8_MAX);
				dos.putInt8(len);
				dos.putInt8Array(keyBytes);
				//BON.writeValue(dos, hm[key]);
				hm[key].write(dos);
			}
		}

		static writeValue(dos, value){
			const v=value.value;
			switch(value.type){
				case C_NULL:
				case C_FALSE:
				case C_TRUE:
					dos.putInt8(value.type);
					return;
				case C_BON:
					value.write(dos);
					return;
				case C_BON_ARRAY:
					value.write(dos);
					return;
				case C_STRING:
					let strBytes=UTF8E.encode(v);
					const len=strBytes.length;
					if(len<=63){
						dos.putInt8(TINYINT_MAX_VALUE+len+1);
						dos.putInt8Array(strBytes);
						return;
					}
					if(len<=INT8_MAX){
						dos.putInt8(C_STRING8);
						dos.putInt8(len);
						dos.putInt8Array(strBytes);
						return;
					}
					if(len<=UINT16_MAX){
						dos.putInt8(C_STRING16);
						dos.putUInt16(len);
						dos.putInt8Array(strBytes);
						return;
					}
					dos.putInt8(C_STRING32);
					dos.putInt32(len);
					dos.putInt8Array(strBytes);
					return;
				case C_BYTEARRAY:
					return;
				case C_INTEGER:
					console.log("putting int "+v);
					if(v>=TINYINT_MIN_VALUE&&v<=TINYINT_MAX_VALUE){
						dos.putInt8(v);
						return;
					}
					if(v>=INT8_MIN&&v<=INT8_MAX){
						dos.putInt8(C_INT8);
						dos.putInt8(v);
						return;
					}
					if(v>=INT16_MIN&&v<=INT16_MAX){
						dos.putInt8(C_INT16);
						dos.putInt16(v);
						return;
					}
					if(v>=INT32_MIN&&v<=INT32_MAX){
						dos.putInt8(C_INT32);
						dos.putInt32(value);
						return;
					}
					dos.putInt8(C_INT64);
					dos.putInt64(v);
					return;
				case C_FLOAT:
					dos.putInt8(C_FLOAT);
					dos.putFloat32(v);
					return;
				case C_DOUBLE:
					dos.putInt8(C_DOUBLE);
					dos.putFloat64(v);
					return;
				default:
					throw new Error("unknown value type: " +value.type);
			}
		}

		getValue() {
			return this;
		}


		quote() {
			return "";
		}


		read(length, dis){
			while(--length>=0){
				let key=this.readKey(dis);
				console.log("key read: "+key);
				const value=BONValue.readValue(dis);
				this.hm[key]=value;
				this.value[key]=value.value;
			}
		}

		readKey(dis){
			return dis.getString(dis.getInt8());
		}

		toString(){
			return JSON.stringify(this.value);
		}

		length() {
			return 0;
		}
	}

	class BONArray extends BONValue{
		constructor(len, dis) {
			super();
			this.array=[len];
			this.value=[len];
			this.read(len, dis);
		}


		read(len, dis) {
			const array=this.array;
			const value=this.value;
			for(let i=0;i<len;++i){
				const _value=BONValue.readValue(dis);
				array[i]=_value;
				value[i]=_value.value;
			}
		}

		write(dos){
			const len=this.array.length;
			if(len<=INT8_MAX){
				dos.putInt8(C_BON_ARRAY8);
				dos.putInt8(len);
				this.writeValues(dos);
				return;
			}
			if(len<=UINT16_MAX){
				dos.putInt8(C_BON_ARRAY16);
				dos.putUInt16(len);
				this.writeValues(dos);
				return;
			}
			dos.putInt8(C_BON_ARRAY32);
			dos.putInt32(len);
			this.writeValues(dos);
		}

		writeValues(dos){
			const array=this.array;
			const len=array.length;
			for(let i=0;i<len;++i){
				BON.writeValue(dos, array[i]);
			}

		}

		get(index){
			return this.value[index];
		}

		getBON(index){
			return this.array[index];
		}


		length(){
			return this.array.length;
		}

		put(value){
		}

		toString(){
			return JSON.stringify(this.value);
		}
	}
	class BONString extends BONValue{
		constructor(value){
			super(value);
		}

		write(dos){
			let strBytes=UTF8E.encode(this.value);
			const len=strBytes.length;
			if(len<=63){
				dos.putInt8(TINYINT_MAX_VALUE+len+1);
				dos.putInt8Array(strBytes);
				return;
			}
			if(len<=INT8_MAX){
				dos.putInt8(C_STRING8);
				dos.putInt8(len);
				dos.putInt8Array(strBytes);
				return;
			}
			if(len<=UINT16_MAX){
				dos.putInt8(C_STRING16);
				dos.putUInt16(len);
				dos.putInt8Array(strBytes);
				return;
			}
			dos.putInt8(C_STRING32);
			dos.putInt32(len);
			dos.putInt8Array(strBytes);

		}
	}
	class BONInteger extends BONValue{
		constructor(value){
			super(value);
		}
		write(dos){
			let value=this.value;
			if(value>=TINYINT_MIN_VALUE&&value<=TINYINT_MAX_VALUE){
				dos.putInt8(value);
				return;
			}
			if(value>=INT8_MIN&&value<=INT8_MAX){
				dos.putInt8(C_INT8);
				dos.putInt8(value);
				return;
			}
			if(value>=INT16_MIN&&value<=INT16_MAX){
				dos.putInt8(C_INT16);
				dos.putInt16(value);
				return;
			}
			if(value>=INT32_MIN&&value<=INT32_MAX){
				dos.putInt8(C_INT32);
				dos.putInt32(value);
				return;
			}
			dos.putInt8(C_INT64);
			dos.putInt64(value);
		}

	}

	class BONFloat extends BONValue{
		constructor(value){
			super(value);
		}
		write(dos){
			dos.putInt8(C_FLOAT);
			dos.putFloat32(this.value);
		}

	}

	class BONDouble extends BONValue{
		constructor(value){
			super(value);
		}
		write(dos){
			dos.putInt8(C_DOUBLE);
			dos.putFloat64(this.value);
		}

	}

	window.BON=BON;
	window.BONArray=BONArray;
	window.readBON=function(arr){
		return BONValue.readValue(new DataInput(arr));
	};

})();
