package org.bon;
import java.util.*;
import java.io.*;
import java.util.zip.*;

public class BON extends BONValue{
	static final byte _NULL=-128;
	static final byte _FALSE=-127;
	static final byte _TRUE=-126;
	static final byte BON8=-125;
	static final byte BON16=-124;
	static final byte BON_ARRAY8=-123;
	static final byte BON_ARRAY16=-122;
	static final byte BON_ARRAY32=-121;
	static final byte STRING8=-120;
	static final byte STRING16=-119;
	static final byte STRING32=-118;
	static final byte BYTEARRAY8=-117;
	static final byte BYTEARRAY16=-116;
	static final byte BYTEARRAY32=-115;
	static final byte INT8=-114;
	static final byte INT16=-113;
	static final byte INT32=-112;
	static final byte INT64=-111;
	static final byte FLOAT=-110;
	static final byte DOUBLE=-109;
	static final byte COMPRESSED_BON=-108;

	static final byte MAX_TINYBON=40;


	final HashMap<String, BONValue> hm=new HashMap<>();
	public BON(){
	}

	public BON(byte[] arr){
		this(new ByteArrayInputStream(arr));
	}

	public BON(String json){
	    readJSONObjectFully(new JSONTokener(json));
	}

	public BON(InputStream is){
			read(is);
	}

	BON(JSONTokener jsonTokener) {
		readJSONObjectFully(jsonTokener);
	}

	private void read(InputStream is){
		try {
			int read=is.read();
			if(read==-1)
				throw new EOFException();
			byte type = (byte)read;
			switch(type){
				case BON8:
					DataInputStream dis = new DataInputStream(is);
					read(dis.readByte(), dis);
					return;
				case BON16:
					dis = new DataInputStream(is);
					read(dis.readChar(), dis);
					return;
				case COMPRESSED_BON:
					InputStream zis=new GZIPInputStream(is);
					read(zis);
					return;
				case '{':
				    readJSONObject(is);
					return;
			}
			if(type<TINYINT_MIN_VALUE){
				DataInputStream dis = new DataInputStream(is);
				read(-(type-TINYINT_MIN_VALUE+1), dis);
				return;
			}
			throw new BONException("not a BON format");
		} catch (IOException e) {
			throw new BONException(e);
		}
	}

	private void readJSONObjectFully(JSONTokener x) {
		if (x.nextClean() != '{') {
			throw x.syntaxError("A JSONObject text must begin with '{'");
		}
		readJSONObject(x);
	}

	private void readJSONObject(InputStream is) {
		JSONTokener x = new JSONTokener(is);
		readJSONObject(x);
	}

	private void readJSONObject(JSONTokener x) {
		char c;
		String key;
		/*
		if (x.nextClean() != '{') {
			throw x.syntaxError("A JSONObject text must begin with '{'");
		}*/
		for (;;) {
			c = x.nextClean();
			switch (c) {
				case 0:
					throw x.syntaxError("A JSONObject text must end with '}'");
				case '}':
					return;
				default:
					x.back();
					key = x.nextValue().toString();
			}

			// The key is followed by ':'.

			c = x.nextClean();
			if (c != ':') {
				throw x.syntaxError("Expected a ':' after a key");
			}

			// Use syntaxError(..) to include error location

			if (key != null) {
				// Check if key exists
				if (hm.containsKey(key)) {
					// key already exists
					throw x.syntaxError("Duplicate key \"" + key + "\"");
				}
				// Only add value if non-null
				Object value = x.nextValue();
				if (value!=null) {
					this.put(key, value);
				}
			}

			// Pairs are separated by ','.

			switch (x.nextClean()) {
				case ';':
				case ',':
					if (x.nextClean() == '}') {
						return;
					}
					x.back();
					break;
				case '}':
					return;
				default:
					throw x.syntaxError("Expected a ',' or '}'");
			}
		}
	}

	BON(int length, DataInputStream is){
		read(length, is);
	}

	static final BONValue NULL=new BONValue(){
		@Override
		public void write(DataOutputStream dos) throws IOException {
		    dos.writeByte(_NULL);
		}

		@Override
		public Object getValue() {
			return null;
		}

		@Override
		public String stringValue() {
			return null;
		}

		@Override
		public byte[] byteArrayValue() {
		    return null;
		}

		@Override
		public String quote() {
			return "null";
		}
	};

	static final BONValue TRUE=new BONValue(){
		@Override
		public void write(DataOutputStream dos) throws IOException {
		    dos.writeByte(_TRUE);
		}

		@Override
		public Object getValue() {
			return Boolean.TRUE;
		}

		@Override
		public String stringValue() {
			return "true";
		}

		@Override
		public byte[] byteArrayValue() {
		    return null;
		}

		@Override
		public String quote() {
			return "true";
		}
	};

	static final BONValue FALSE=new BONValue(){
		@Override
		public void write(DataOutputStream dos) throws IOException {
		    dos.writeByte(_FALSE);
		}

		@Override
		public Object getValue() {
			return Boolean.FALSE;
		}

		@Override
		public String stringValue() {
			return "false";
		}

		@Override
		public byte[] byteArrayValue() {
		    return null;
		}

		@Override
		public String quote() {
			return "false";
		}
	};

	public final BON put(String key, Object value){
		if(value==null){
		    hm.put(key, NULL);
			return this;
		}
		switch (value.getClass().getName()){
			case "java.lang.Byte":
				return put(key, (byte)value);
			case "java.lang.Short":
				return put(key, (short)value);
			case "java.lang.Integer":
				return put(key, (int)value);
			case "java.lang.Long":
				return put(key, (long)value);
			case "java.lang.Float":
				return put(key, (float)value);
			case "java.lang.Double":
				return put(key, (double)value);
			case "java.lang.String":
				return put(key, (String) value);
			case "java.lang.Boolean":
				return put(key, (boolean) value);
			case "org.bon.BON":
				return put(key, (BON) value);
			case "org.bon.BONArray":
				return put(key, (BONArray) value);
			default:throw new BONException("cant store object of type: "+value.getClass().getName());
		}
	}

	public final BON put(String key, boolean value){
		hm.put(key, value?TRUE:FALSE);
		return this;
	}

	public final BON put(String key, int value){
		hm.put(key, new BONInteger(value));
		return this;
	}

	public final BON put(String key, float value){
		hm.put(key, new BONFloat(value));
		return this;
	}
		
	public final BON put(String key, double value){
		hm.put(key, new BONDouble(value));
		return this;
	}

	public final BON put(String key, long value){
		hm.put(key, new BONInteger(value));
		return this;
	}

	public final BON put(String key, BON value){
		if(value==null){
		    hm.put(key, NULL);
			return this;
		}
		hm.put(key, value);
		return this;
	}

	public final BON put(String key, BONArray value){
		if(value==null){
		    hm.put(key, NULL);
			return this;
		}
		hm.put(key, value);
		return this;
	}

	public final BON put(String key, String value){
		if(value==null)
			hm.put(key, NULL);
		else
			hm.put(key, new BONString(value));
		return this;
	}

	public final BON put(String key, byte[] value){
		if(value==null)
			hm.put(key, NULL);
		else
			hm.put(key, new BONByteArray(value));
		return this;
	}

	private BONValue getValue(String key){
		BONValue value = hm.get(key);
		if(value==null)
			throw new RuntimeException("no such key: "+key);
		return value;
	}

	public final Object get(String key){
		BONValue value = hm.get(key);
		return value==null?null:value.getValue();
	}

	public final boolean getBoolean(String key){
		return (boolean)getValue(key).getValue();
	}

	public final int getInt(String key){
		return getValue(key).intValue();
	}

	public final float getFloat(String key){
		return getValue(key).floatValue();
	}

	public final double getDouble(String key){
		return getValue(key).doubleValue();
	}

	public final long getLong(String key){
		return getValue(key).longValue();
	}

	public final String getString(String key){
		BONValue value = hm.get(key);
		return value==null?null:value.stringValue();
	}

	public final BON getBON(String key){
		BONValue value = hm.get(key);
		return value==null||value==NULL?null:(BON)value;
	}

	public final BONArray getBONArray(String key){
		BONValue value = hm.get(key);
		return value==null||value==NULL?null:(BONArray)value;
	}

	public final byte[] getByteArray(String key){
		BONValue value = hm.get(key);
		return value==null?null:value.byteArrayValue();
	}

	public final void remove(String key){
		hm.remove(key);
	}

	public final boolean has(String key){
		return hm.containsKey(key);
	}


	public byte[] toByteArray(){
		ByteArrayOutputStream baos=new ByteArrayOutputStream();
		write(baos);
		return baos.toByteArray();
	}

	public byte[] toCompressedByteArray(){
		ByteArrayOutputStream baos=new ByteArrayOutputStream();
		baos.write(COMPRESSED_BON);
		GZIPOutputStream zos;
		try {
			zos = new GZIPOutputStream(baos);
		} catch (IOException e) {
		    throw new RuntimeException(e);
		}
		write(zos);
		try {
			zos.finish();
			zos.close();
			baos.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return baos.toByteArray();
	}

	public void write(OutputStream os){
		DataOutputStream dos=new DataOutputStream(os);
		write(dos);
	}

	protected void write(DataOutputStream dos){
		int length=hm.size();
		Iterator<Map.Entry<String, BONValue>> it = hm.entrySet().iterator();
		try{
			if(length<MAX_TINYBON){
				dos.writeByte(-length+TINYINT_MIN_VALUE-1);
			}
			else if(length<=Byte.MAX_VALUE){
				dos.writeByte(BON8);
				dos.writeByte(length);
			}
			else if(length<=Character.MAX_VALUE){
				dos.writeByte(BON16);
				dos.writeChar(length);
			}
			while (it.hasNext()) {
				Map.Entry<String, BONValue> pair = it.next();
				byte[] key=pair.getKey().getBytes();
				int len=key.length;
				if(len>Byte.MAX_VALUE)throw new BONException("max key length for a BON is "+Byte.MAX_VALUE);
				dos.writeByte(len);
				dos.write(key);
				pair.getValue().write(dos);
			}
			//dos.write(END);
		}
		catch(IOException e){
			throw new BONException(e);
		}
	
	}

	@Override
	public Object getValue() {
		return this;
	}

	@Override
	public String stringValue() {
		return null;
	}

	@Override
	public byte[] byteArrayValue() {
		return new byte[0];
	}

	@Override
	public String quote() {
		return toString();
	}



	private void read(int length, DataInputStream dis){
		while(--length>=0){
			hm.put(readKey(dis), BONValue.readValue(dis));
		}
	}

	private String readKey(DataInputStream dis){
		try{	
			int len=dis.readByte();
			if(len==0)
				return "";
			byte[] key =new byte[len];
			dis.readFully(key);
			return new String(key);
		}
		catch(IOException e){
			throw new BONException("can not read key", e);
		}
	}

	public String toString(){
		StringBuilder sb=new StringBuilder();
		sb.append("{");
		Iterator<Map.Entry<String, BONValue>> it = hm.entrySet().iterator();
		String prefix="";
		while (it.hasNext()) {
			Map.Entry<String, BONValue> pair = it.next();
			sb
					.append(prefix)
					.append(BONString.quote(pair.getKey()))
					.append(":")
					.append(pair.getValue().quote());
			prefix=",";
		}

		return sb.append("}").toString();
	}

	public Set<String> keySet(){
		return hm.keySet();
	}

	public final int length() {
		return this.hm.size();
	}

	public final boolean isEmpty() {
		return this.hm.isEmpty();
	}
	public static void main(String ... args)throws IOException{
		System.out.println(new BON(new FileInputStream(args[0])));
	}
}
