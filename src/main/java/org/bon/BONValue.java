package org.bon;
/*
    created by neoexpert at 28.01.21
*/

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import static org.bon.BON.*;

abstract class BONValue extends Number {
	static final byte TINYINT_MIN_VALUE=-64;
	static final byte TINYINT_MAX_VALUE=63;

    static BONValue readValue(DataInputStream dis){
        try{
            int type=dis.readByte();
						if(type<TINYINT_MIN_VALUE)
            switch(type){
                case _FALSE:
                    return BON.FALSE;
                case _TRUE:
                    return BON.TRUE;
                case BON8:
                    return new BON(dis.readByte(), dis);
                case BON16:
                    return new BON(dis.readChar(), dis);
                case BON_ARRAY8:
                    return new BONArray(dis.readByte(), dis);
                case BON_ARRAY16:
                    return new BONArray(dis.readChar(), dis);
                case BON_ARRAY32:
                    return new BONArray(dis.readInt(), dis);
                case STRING8:
                    int len = dis.readByte();
                    byte[] arr =new byte[len];
                    dis.readFully(arr);
                    return new BONString(new String(arr));
                case STRING16:
                    len=dis.readChar();
                    arr =new byte[len];
                    dis.readFully(arr);
                    return new BONString(new String(arr));
                case STRING32:
                    len=dis.readInt();
                    arr =new byte[len];
                    dis.readFully(arr);
                    return new BONString(new String(arr));
                case BYTEARRAY8:
                    len=dis.readByte();
                    arr=new byte[len];
                    dis.readFully(arr);
                    return new BONByteArray(arr);
                case BYTEARRAY16:
                    len=dis.readChar();
                    arr=new byte[len];
                    dis.readFully(arr);
                    return new BONByteArray(arr);
                case BYTEARRAY32:
                    len=dis.readInt();
                    arr=new byte[len];
                    dis.readFully(arr);
                    return new BONByteArray(arr);
                case INT8:
                    return BONInteger.valueOf(dis.readByte());
                case INT16:
                    return new BONInteger(dis.readShort());
                case INT32:
                    return new BONInteger(dis.readInt());
                case INT64:
                    return new BONInteger(dis.readLong());
                case FLOAT:
                    return new BONFloat(dis.readFloat());
                case DOUBLE:
                    return new BONDouble(dis.readDouble());
                case _NULL:
                    return BON.NULL;
								default:
                    return new BON(-(type-TINYINT_MIN_VALUE+1), dis);
            }
						if(type<=TINYINT_MAX_VALUE)
            	return new BONInteger(type);
						byte[] arr =new byte[type-TINYINT_MAX_VALUE-1];
						dis.readFully(arr);
						return new BONString(new String(arr));
        }
        catch(IOException e){
            throw new BONException("can not read value", e);
        }
    }

    protected abstract void write(DataOutputStream dos)throws IOException;
    public abstract Object getValue();

    public abstract String stringValue();

    public abstract byte[] byteArrayValue();

    public abstract String quote();

    public double doubleValue() {
        throw new ClassCastException();
    }

    public float floatValue() {
        throw new ClassCastException();
    }

    public long longValue() {
        throw new ClassCastException();
    }

    public static BONValue valueOf(Object value){
        if(value==null)
            return NULL;
        switch (value.getClass().getName()){
            case "java.lang.Byte":
            case "java.lang.Short":
            case "java.lang.Integer":
            case "java.lang.Long":
                return new BONInteger(((Number)value).longValue());
            case "java.lang.Float":
                return new BONFloat((Float) value);
            case "java.lang.Double":
                return new BONDouble((Double) value);
            case "java.lang.String":
                return new BONString((String) value);
            case "java.lang.Boolean":
                if(value==Boolean.TRUE)
                    return TRUE;
                else return FALSE;
            case "org.bon.BON":
            case "org.bon.BONArray":
                return (BONValue) value;
            default:throw new BONException("cant store object of type: "+value.getClass().getName());
        }
    }

    public int intValue() {
        throw new ClassCastException();
    }

		public boolean equals(Object o){
			if(o==null)
				return false;
			if(o instanceof BONValue)
				return ((BONValue)o).getValue().equals(getValue());
			return getValue().equals(o);
		}
}
