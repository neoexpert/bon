package org.bon;
/*
    created by neoexpert at 28.01.21
*/

import java.io.DataOutputStream;
import java.io.IOException;

import static org.bon.BON.*;

class BONByteArray extends BONValue {
    private final byte[] value;

    public BONByteArray(byte[] value) {
        this.value = value;
    }

    @Override
    public void write(DataOutputStream dos) throws IOException {
        byte[] arr=value;
        int len=arr.length;
        if(len<=Byte.MAX_VALUE){
            dos.writeByte(BYTEARRAY8);
            dos.writeByte(len);
            dos.write(arr);
            return;
        }
        if(len<=Character.MAX_VALUE){
            dos.writeByte(BYTEARRAY16);
            dos.writeChar(len);
            dos.write(arr);
            return;
        }
        dos.writeByte(BYTEARRAY32);
        dos.writeInt(len);
        dos.write(arr);
    }

    @Override
    public final Object getValue() {
        return value;
    }


    @Override
    public final String stringValue() {
        throw new ClassCastException("byte[] cannot be converted to String");
    }

    @Override
    public byte[] byteArrayValue() {
        return value;
    }


    @Override
    public String quote() {
        return "byte[]";
    }
}
