package org.bon;
/*
    created by neoexpert at 28.01.21
*/

import java.io.DataOutputStream;
import java.io.IOException;

class BONFloat extends BONValue {
    private final float value;

    public BONFloat(float value) {
        this.value = value;
    }

    @Override
    public void write(DataOutputStream dos) throws IOException {
        dos.writeByte(BON.FLOAT);
        dos.writeFloat(value);
    }

    @Override
    public final Object getValue() {
        return value;
    }

    @Override
    public final float floatValue() {
        return value;
    }

    @Override
    public final String stringValue() {
        throw new ClassCastException("float cannot be converted to String");
    }

    @Override
    public byte[] byteArrayValue() {
        throw new ClassCastException("float cannot be converted to byte[]");
    }

    private String quoted;

    @Override
    public String quote() {
        if (quoted != null)
            return quoted;
        quoted = Float.toString(value);
        return quoted;
    }
}
