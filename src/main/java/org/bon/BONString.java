package org.bon;
/*
    created by neoexpert at 28.01.21
*/

import java.io.DataOutputStream;
import java.io.IOException;

import static org.bon.BON.*;

class BONString extends BONValue {
    private final String value;

    public BONString(String value) {
        this.value = value;
    }

    @Override
    public void write(DataOutputStream dos) throws IOException {
        byte[] key=value.getBytes();
        int len=key.length;
        if(len<=63){
            dos.writeByte(TINYINT_MAX_VALUE+len+1);
            dos.write(key);
            return;
        }
        if(len<=Byte.MAX_VALUE){
            dos.writeByte(STRING8);
            dos.writeByte(len);
            dos.write(key);
            return;
        }
        if(len<=Character.MAX_VALUE){
            dos.writeByte(STRING16);
            dos.writeChar(len);
            dos.write(key);
            return;
        }
        dos.writeByte(STRING32);
        dos.writeInt(len);
        dos.write(key);
    }

    @Override
    public final Object getValue() {
        return value;
    }


    @Override
    public final String stringValue() {
        return value;
    }

    @Override
    public byte[] byteArrayValue() {
        return value.getBytes();
    }

    private String quoted;

    @Override
    public String quote() {
        if (quoted != null)
            return quoted;
        quoted=quote(value);
        return quoted;
    }

    public static String quote(String value){
        StringBuilder sb=new StringBuilder();
        sb.append("\"");
        int len=value.length();
        char c=0;
        for(int i = 0; i < len; ++i) {
            char b = c;
            c = value.charAt(i);
            switch(c) {
                case '\b':
                    sb.append("\\b");
                    continue;
                case '\t':
                    sb.append("\\t");
                    continue;
                case '\n':
                    sb.append("\\n");
                    continue;
                case '\f':
                    sb.append("\\f");
                    continue;
                case '\r':
                    sb.append("\\r");
                    continue;
                case '"':
                case '\\':
                    sb.append('\\');
                    sb.append(c);
                    continue;
                case '/':
                    if (b == '<') {
                        sb.append('\\');
                    }

                    sb.append(c);
                    continue;
            }

            if (c >= ' ' && (c < 128 || c >= 160) && (c < 8192 || c >= 8448)) {
                sb.append(c);
            } else {
                sb.append("\\u");
                String hhhh = Integer.toHexString(c);
                sb.append("0000", 0, 4 - hhhh.length());
                sb.append(hhhh);
            }
        }
        sb.append("\"");
        return sb.toString();
    }
}
