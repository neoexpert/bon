package org.bon;
/*
    created by neoexpert at 28.01.21
*/

import java.io.DataOutputStream;
import java.io.IOException;

import static org.bon.BON.*;

class BONInteger extends BONValue {
    private final long value;

    public BONInteger(long value) {
        this.value = value;
    }

    @Override
    public void write(DataOutputStream dos) throws IOException {
        if(value>=TINYINT_MIN_VALUE&&value<=TINYINT_MAX_VALUE){
            dos.writeByte((byte)value);
            return;
        }
        if(value>=Byte.MIN_VALUE&&value<=Byte.MAX_VALUE){
            dos.writeByte(INT8);
            dos.writeByte((byte)value);
            return;
        }
        if(value>=Short.MIN_VALUE&&value<=Short.MAX_VALUE){
            dos.writeByte(INT16);
            dos.writeShort((short)value);
            return;
        }
        if(value>=Integer.MIN_VALUE&&value<=Integer.MAX_VALUE){
            dos.writeByte(INT32);
            dos.writeInt((int)value);
            return;
        }
        dos.writeByte(INT64);
        dos.writeLong(value);
    }

    @Override
    public final Object getValue() {
        return value;
    }

    @Override
    public final long longValue() {
        return value;
    }


    @Override
    public final String stringValue() {
        throw new ClassCastException("(long) integer cannot be converted to String");
    }

    @Override
    public byte[] byteArrayValue() {
        throw new ClassCastException("(long) integer cannot be converted to byte[]");
    }

    public double doubleValue() {
        return value;
    }

    public float floatValue() {
        return value;
    }

    public int intValue() {
        return (int) value;
    }

    private String quoted;

    @Override
    public String quote() {
        if (quoted != null)
            return quoted;
        quoted = Long.toString(value);
        return quoted;
    }

	private static final int LOW = -128;
	private static final int HIGH=127;
	private static final int CACHE_LENGTH=(HIGH - LOW) + 1;
	private static final BONInteger[] intcache=new BONInteger[CACHE_LENGTH];
		static {

			int j = LOW;
			for(int k = 0; k < CACHE_LENGTH; ++k){
				intcache[k] = new BONInteger(j);
				++j;
			}

		}
		public static BONInteger valueOf(final byte i){
			return intcache[i - LOW];
		}
}
