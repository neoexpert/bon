package org.bon;
/*
    created by neoexpert at 28.01.21
*/

public class BONException extends RuntimeException{
    public BONException(){}
    public BONException(String message){
        super(message);
    }

    public BONException(Throwable cause){
        super(cause);
    }

    public BONException(String message, Throwable cause){
        super(message, cause);
    }
}
