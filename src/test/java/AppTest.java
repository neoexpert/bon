
import org.bon.*;
import org.json.*;

import java.util.*;
import java.io.*;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase {
	/**
	 * Create the test case
	 *
	 * @param testName name of the test case
	 */
	public AppTest(final String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(AppTest.class);
	}

	public void testEmptyBON()throws IOException{
		new BON(new BON().toByteArray());
	}

	public void testJSON(){
		BON empty=new BON("{}");
		assertTrue(empty.isEmpty());
		BON b1=new BON("{\"key\":\"value\"}");
		assertEquals("value",b1.getString("key"));
		BON b2=new BON("{\"key\":42}");
		assertEquals(42,b2.getInt("key"));
		String json="{\"key1\":42, \"key2\":{\"key\":43}}";
		JSONObject jo=new JSONObject(json);
		BON b3=new BON(json);
		assertEquals(42,b3.getInt("key1"));
		assertEquals(43,b3.getBON("key2").getInt("key"));
		json="{\"key1\":[null, null, true, false, {}, [null, {\"key\":true}]]}";
		BON b4=new BON(json);
		System.out.println(b4);
		json="{\"body\":[[123,3,89.95,true,null,null,\"ASDF-0002233\"],[4711,10,12.49,false,\"1991-01-09 07:45:00.0\",\"2002-02-02\",\"ASDF-004711\"]]}";
		b4=new BON(json);
		System.out.println(b4);
	}

	public void testMinBON(){
		assertEquals(new BON(new BON().put("",0).toByteArray()).getInt(""), 0);
	}

	public void testBONArray() {
		BONArray bonArray = new BONArray();
		bonArray
				.put("string")
				.put(42.5f)
				.put(42.5)
				.put(42L)
				.put(true)
				.put((Object) null)
				.put(new BON().put("key", "value"))
				.put(42);
		bonArray = new BONArray(bonArray.toByteArray());
		assertEquals("string", bonArray.getString(0));
		assertEquals(42.5f, bonArray.getFloat(1));
		assertEquals(42.5, bonArray.getDouble(2));
		assertEquals(42L, bonArray.getLong(3));
		assertEquals(true, bonArray.getBoolean(4));
		assertNull(bonArray.get(5));
		BON bon = bonArray.getBON(6);
		assertEquals("value", bon.getString("key"));
		assertEquals(42, bonArray.getInt(7));
	}

	public void testBON() throws IOException{
		BON jo = new BON();
		jo.put("key", "value");
		jo.put("float", 42.0f);
		jo.put("int", 42);
		jo.put("long", 42L);
		jo.put("double", 42.0);
		jo.put("null", (Object) null);
		BON subJO = new BON();
		subJO.put("key3", "value3");
		jo.put("sub", subJO);
		BONArray bonArray = new BONArray();
		bonArray.put(new BON().put("key", "value"));
		bonArray.put(42);
		bonArray.put(42.5f);
		bonArray.put(42.5);
		bonArray.put("foo quote: \" unicode: Д");
		bonArray.put(true);
		bonArray.put(new BONArray().put(1).put(2).put(3));
		jo.put("array", bonArray);
		System.out.println(jo);
		jo = new BON(jo.toByteArray());
		System.out.println(jo);
	}

	public void testBONAndValidate() {
		Random r = new Random();
		long seed = r.nextLong();

		r = new Random(seed);
		BON jo = new BON();
		int keys = r.nextInt(256) + 128;

		for (int i = 0; i < keys; ++i) {
			switch (r.nextInt(9)) {
				case 0:
					jo.put("key" + i, "value" + i);
					break;
				case 1:
					jo.put("boolean" + i, r.nextBoolean());
					break;
				case 2:
					jo.put("float" + i, r.nextFloat());
					break;
				case 3:
					jo.put("int" + i, r.nextInt());
					break;
				case 4:
					jo.put("long" + i, r.nextLong());
					break;
				case 5:
					jo.put("null" + i, (Object) null);
					break;
				case 6:
					jo.put("double" + i, r.nextDouble());
					break;
				case 7:
					byte[] bvalue = new byte[r.nextInt(128)];
					r.nextBytes(bvalue);
					jo.put("barray" + i, bvalue);
					break;
				case 8:
					int arraylen = r.nextInt(128);
					BONArray arr = new BONArray();
					for (int j = 0; j < arraylen; ++j) {
						arr.put(r.nextBoolean());
						arr.put(new BONArray().put("value1").put("value2"));
						arr.put(r.nextInt());
						arr.put(new BON().put("key", "value"));
						arr.put(r.nextLong());
						arr.put(r.nextDouble());
						arr.put(r.nextFloat());
					}
					jo.put("bonarray" + i, arr);
					break;
			}
		}
		BON subJO = new BON();
		subJO.put("key3", "value3");
		jo.put("sub", subJO);

		String rndStr=randomString(r,Byte.MAX_VALUE+1);
		jo.put("rnd16String",rndStr);
		rndStr=randomString(r,Character.MAX_VALUE+1);
		jo.put("rnd32String",rndStr);

		BONArray rndArr=randomBONArray(r,Byte.MAX_VALUE+1);
		jo.put("rnd16Array",rndArr);
		rndArr=randomBONArray(r,Character.MAX_VALUE+1);
		jo.put("rnd32Array",rndArr);

		byte[] bvalue = new byte[Byte.MAX_VALUE+1];
		r.nextBytes(bvalue);
		jo.put("rnd16ByteArray", bvalue);

		bvalue = new byte[Character.MAX_VALUE+1];
		r.nextBytes(bvalue);
		jo.put("rnd32ByteArray", bvalue);


		for(int i=-128;i<128;++i){
			jo.put("smallInt"+i, i);
		}
		for(int i=0;i<128;++i){
			jo.put("smallStr"+i, aString(i));
		}

		byte[] raw = jo.toByteArray();
		jo = new BON(raw);

		r = new Random(seed);
		keys = r.nextInt(256) + 128;

		for (int i = 0; i < keys; ++i) {
			switch (r.nextInt(9)) {
				case 0:
					assertEquals("value" + i, jo.get("key" + i));
					break;
				case 1:
					assertEquals(r.nextBoolean(), jo.get("boolean" + i));
					break;
				case 2:
					assertEquals(r.nextFloat(), jo.get("float" + i));
					break;
				case 3:
					assertEquals(r.nextInt(), jo.getInt("int" + i));
					break;
				case 4:
					assertEquals(r.nextLong(), jo.get("long" + i));
					break;
				case 5:
					assertTrue(jo.has("null" + i));
					assertNull(jo.get("null" + i));
					break;
				case 6:
					assertEquals(r.nextDouble(), jo.get("double" + i));
					break;
				case 7:
					byte[] barr = jo.getByteArray("barray" + i);
					assertEquals(r.nextInt(128), barr.length);
					bvalue = new byte[barr.length];
					r.nextBytes(bvalue);
					assertTrue(Arrays.equals(barr, bvalue));
					break;
				case 8:
					BONArray arr = jo.getBONArray("bonarray" + i);
					int arraylen = r.nextInt(128);
					assertEquals(arr.length(), arraylen * 7);
					for (int j = 0; j < arraylen; ++j) {
						assertEquals(r.nextBoolean(), arr.getBoolean(j * 7));
						BONArray subarr = arr.getBONArray(j * 7 + 1);
						assertEquals("value1", subarr.getString(0));
						assertEquals("value2", subarr.getString(1));
						assertEquals(r.nextInt(), arr.getInt(j * 7 + 2));
						BON subbon = arr.getBON(j * 7 + 3);
						assertEquals("value", subbon.getString("key"));
						assertEquals(r.nextLong(), arr.getLong(j * 7 + 4));
						assertEquals(r.nextDouble(), arr.getDouble(j * 7 + 5));
						assertEquals(r.nextFloat(), arr.getFloat(j * 7 + 6));
					}
					break;
			}


		}
		subJO = jo.getBON("sub");
		assertEquals("value3", subJO.get("key3"));
		assertNull(jo.get("unknown"));
		assertNull(jo.getString("unknown"));
		assertNull(jo.getBON("unknown"));
		assertNull(jo.getByteArray("unknown"));

		assertEquals(jo.getString("rnd16String"),randomString(r,Byte.MAX_VALUE+1));
		assertEquals(jo.getString("rnd32String"),randomString(r,Character.MAX_VALUE+1));
		randomBONArrayValidate(jo.getBONArray("rnd16Array"),r,Byte.MAX_VALUE+1);
		randomBONArrayValidate(jo.getBONArray("rnd32Array"),r,Character.MAX_VALUE+1);

		byte[] barr = jo.getByteArray("rnd16ByteArray");
		bvalue = new byte[barr.length];
		r.nextBytes(bvalue);
		assertTrue(Arrays.equals(barr, bvalue));

		barr = jo.getByteArray("rnd32ByteArray");
		bvalue = new byte[barr.length];
		r.nextBytes(bvalue);
		assertTrue(Arrays.equals(barr, bvalue));

		for(int i=-128;i<128;++i){
			assertEquals(jo.getInt("smallInt"+i), i);
		}
		for(int i=0;i<128;++i){
			assertEquals(jo.getString("smallStr"+i), aString(i));
		}
	}

	/**
	 *
	 */
	public int testBON(Random r) {
		BON jo = new BON();
		int keys = r.nextInt(128);

		for (int i = 0; i < keys; ++i) {
			jo.put("key" + i, "value" + i);
			jo.put("float" + i, r.nextFloat());
			jo.put("int" + i, r.nextInt());
			jo.put("long" + i, r.nextLong());
			jo.put("double" + i, r.nextDouble());
		}
		BON subJO = new BON();
		subJO.put("key3", "value3");
		jo.put("sub", subJO);
		byte[] arr = jo.toByteArray();
		jo = new BON(arr);
		return arr.length;
	}

	public int testJSON(Random r) {
		JSONObject jo = new JSONObject();
		int keys = r.nextInt(128);

		for (int i = 0; i < keys; ++i) {
			jo.put("key" + i, "value" + i);
			jo.put("float" + i, r.nextFloat());
			jo.put("int" + i, r.nextInt());
			jo.put("long" + i, r.nextLong());
			jo.put("double" + i, r.nextDouble());
		}
		JSONObject subJO = new JSONObject();
		subJO.put("key3", "value3");
		jo.put("sub", subJO);
		String s = jo.toString();
		jo = new JSONObject(s);
		return s.length();
	}

	public void testPerformance() {
		Random r = new Random();
		long seed = r.nextLong();
		r = new Random(seed);
		if (r.nextBoolean()) {
			testJSONIntensive(1024, new Random());
			testBONIntensive(1024, new Random());
		} else {
			testBONIntensive(1024, new Random());
			testJSONIntensive(1024, new Random());
		}
		if (r.nextBoolean()) {
			for (int i = 0; i < 4; ++i) {
				seed = r.nextLong();
				testJSONPerformance(1024 * 4, new Random(seed));
				testBONPerformance(1024 * 4, new Random(seed));
			}
		} else {
			for (int i = 0; i < 4; ++i) {
				seed = r.nextLong();
				testBONPerformance(1024 * 4, new Random(seed));
				testJSONPerformance(1024 * 4, new Random(seed));
			}
		}
	}

	private double bonAVG = 0;
	private int bonCount = 0;
	private double jsonAVG = 0;
	private int jsonCount = 0;

	public void testJSONPerformance(int count, Random r) {
		++jsonCount;
		long start = System.nanoTime();
		double avglen = testJSONIntensive(count, r);
		long duration = System.nanoTime() - start;
		jsonAVG += ((double) duration / 1000000.0 - jsonAVG) / jsonCount;
		System.out.println("JSON time: " + duration);
		System.out.println("JSON avgtime: " + jsonAVG + " ns");
		System.out.println("JSON avglen: " + avglen);
	}

	public void testBONPerformance(int count, Random r) {
		++bonCount;
		long start = System.nanoTime();
		double avglen = testBONIntensive(count, r);
		long duration = System.nanoTime() - start;
		bonAVG += ((double) duration / 1000000.0 - bonAVG) / bonCount;
		System.out.println(" BON time: " + duration);
		System.out.println(" BON avgtime: " + bonAVG + " ns");
		System.out.println(" BON avglen: " + avglen);
	}

	public double testBONIntensive(int count, Random r) {
		double length = 0;
		for (int i = 0; i < count; ++i)
			length += testBON(r);
		return length /= count;

	}

	public double testJSONIntensive(int count, Random r) {
		double length = 0;
		for (int i = 0; i < count; ++i)
			length += testJSON(r);
		return length /= count;

	}

	public void testBONToString() {
		assertEquals(new JSONObject(new BON().toString()).toString(), new JSONObject().toString());
		assertEquals(new JSONObject(new BON().put("key", "value").toString()).toString(), new JSONObject().put("key", "value").toString());
		assertEquals(new JSONObject(new BON().put("intKey", 42).toString()).toString(), new JSONObject().put("intKey", 42).toString());
		assertEquals(new JSONObject(new BON().put("floatKey", 42.2f).toString()).toString(), new JSONObject().put("floatKey", 42.2f).toString());
		assertEquals(new JSONObject(new BON().put("objKey", new BON().put("key","value")).toString()).toString(), new JSONObject().put("objKey", new JSONObject().put("key", "value")).toString());
	}

	public void testBONArrayToString() {
		assertEquals(new JSONArray(new BONArray().toString()).toString(), new JSONArray().toString());
		assertEquals(new JSONArray(new BONArray()
				.put("value")
				.put(42)
				.put(42.5f)
				.put(42.5)
				.put(42L)
				.put(new BON().put("key","value"))
				.put(new BONArray().put(1).put(2).put(3))
				.toString()).toString(), new JSONArray()
				.put("value")
				.put(42)
				.put(42.5f)
				.put(42.5)
				.put(42L)
				.put(new JSONObject().put("key","value"))
				.put(new JSONArray().put(1).put(2).put(3))
				.toString());
	}

	String aString(int len){
		StringBuilder sb = new StringBuilder(len);
		for(int i = 0; i < len; i++)
			sb.append('A');
		return sb.toString();
	}

	static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	String randomString(Random rnd, int len){
		StringBuilder sb = new StringBuilder(len);
		for(int i = 0; i < len; i++)
			sb.append(AB.charAt(rnd.nextInt(AB.length())));
		return sb.toString();
	}

	BONArray randomBONArray(Random rnd, int len){
		BONArray arr = new BONArray();
		for(int i = 0; i < len; i++)
		    arr.put(rnd.nextInt());
		return arr;
	}

	void randomBONArrayValidate(BONArray arr, Random rnd, int len){
		for(int i = 0; i < len; i++)
			assertEquals(arr.getInt(i),rnd.nextInt());
	}
	public void testCompressedBON() {
		BON jo = new BON();
		jo.put("key", "value");
		jo.put("float", 42.0f);
		jo.put("int", 42);
		jo.put("long", 42L);
		jo.put("double", 42.0);
		jo.put("null", (Object) null);
		BON subJO = new BON();
		subJO.put("key3", "value3");
		jo.put("sub", subJO);
		BONArray bonArray = new BONArray();
		bonArray.put(new BON().put("key", "value"));
		bonArray.put(42);
		bonArray.put(42.5f);
		bonArray.put(42.5);
		bonArray.put("foo foo foo foo foo foo quote: \" unicode: Д");
		bonArray.put(true);
		bonArray.put(new BONArray().put(1).put(2).put(3));
		jo.put("array", bonArray);
		System.out.println(jo);
		jo = new BON(jo.toCompressedByteArray());
		System.out.println(jo);
		byte[] compressed=jo.toCompressedByteArray();
		byte[] notCompressed=jo.toByteArray();
		double compresionRate=compressed.length/(double)notCompressed.length;
		System.out.println(String.format("compression size: %.2f %%", compresionRate*100.0));
	}
}
