package org.bon;
/*
    created by neoexpert at 28.01.21
*/

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import static org.bon.BON.*;

public class BONArray extends BONValue implements Collection<Object>{
    ArrayList<BONValue> array=new ArrayList<>();

    public BONArray() {
    }

    public BONArray(byte[] arr){
        this(new ByteArrayInputStream(arr));
    }

    public BONArray(String json){
        readJSONArrayFully(new JSONTokener(json));
    }

    BONArray(int len, DataInputStream dis) {
        read(len, dis);
    }

    public BONArray(InputStream is) {
        DataInputStream dis = new DataInputStream(is);
        try {
            byte type=dis.readByte();
            switch (type){
                case BON_ARRAY8:
                    read(dis.readByte(), dis);
                    return;
                case BON_ARRAY16:
                    read(dis.readChar(), dis);
                    return;
                case BON_ARRAY32:
                    read(dis.readInt(), dis);
                    return;
                default:
                    throw new BONException("not a BON format");
            }
        } catch (IOException e) {
            throw new BONException(e);
        }
    }

    BONArray(JSONTokener jsonTokener) {
        readJSONArrayFully(jsonTokener);
    }

    private void readJSONArrayFully(JSONTokener x) {
        if (x.nextClean() != '[') {
            throw x.syntaxError("A JSONArray text must start with '['");
        }
        readJSONArray(x);
    }

    private void readJSONArray(JSONTokener x) {
        /*
        if (x.nextClean() != '[') {
            throw x.syntaxError("A JSONArray text must start with '['");
        }*/

        char nextChar = x.nextClean();
        if (nextChar == 0) {
            // array is unclosed. No ']' found, instead EOF
            throw x.syntaxError("Expected a ',' or ']'");
        }
        if (nextChar != ']') {
            x.back();
            for (;;) {
                if (x.nextClean() == ',') {
                    x.back();
                    this.array.add(BON.NULL);
                } else {
                    x.back();
                    put(x.nextValue());
                }
                switch (x.nextClean()) {
                    case 0:
                        // array is unclosed. No ']' found, instead EOF
                        throw x.syntaxError("Expected a ',' or ']'");
                    case ',':
                        nextChar = x.nextClean();
                        if (nextChar == 0) {
                            // array is unclosed. No ']' found, instead EOF
                            throw x.syntaxError("Expected a ',' or ']'");
                        }
                        if (nextChar == ']') {
                            return;
                        }
                        x.back();
                        break;
                    case ']':
                        return;
                    default:
                        throw x.syntaxError("Expected a ',' or ']'");
                }
            }
        }
    }

    private void read(int len, DataInputStream dis) {
        for(int i=0;i<len;++i)
            array.add(BONValue.readValue(dis));
    }


    public final int length(){
        return array.size();
    }

		@Override
    public final boolean add(Object value){
			put(value);
			return true;
		}

		@Override
    public final void clear(){
			array.clear();
		}

		@Override
		public final boolean retainAll(Collection<?> c){
			return array.retainAll(c);
		}

		@Override
		public final boolean removeAll(Collection<?> c){
			return array.removeAll(c);
		}

		@Override
		public final boolean addAll(Collection<?> c){
			for(Object o:c)
				put(o);
			return true;
		}

		@Override
		public final boolean containsAll(Collection<?> c){
			return array.containsAll(c);
		}

		@Override
		public final boolean remove(Object o){
			return array.remove(o);
		}

		@Override
		public final boolean contains(Object o){
			return array.contains(BONValue.valueOf(o));
		}

		@Override
		public final boolean isEmpty(){
			return array.isEmpty();
		}

		@Override
		public final <T> T[]	toArray(T[] a){
			return array.toArray(a);
		}

		@Override
		public final Object[] toArray(){
			return array.toArray();
		}

		@Override
		public final Iterator<Object> iterator(){
			Iterator<BONValue> it=array.iterator();
			return new Iterator<Object>(){
				@Override
				public final boolean hasNext(){
					return it.hasNext();
				}

				@Override
				public final Object next(){
					return it.next().getValue();
				}
			};
		}

		@Override
		public final int size(){
			return array.size();
		}

    public final BONArray put(Object value){
        if(value==null){
            array.add(NULL);
            return this;
        }
        switch (value.getClass().getName()){
            case "java.lang.Byte":
            case "java.lang.Short":
            case "java.lang.Integer":
            case "java.lang.Long":
                return put(((Number)value).longValue());
            case "java.lang.Float":
                return put((float)value);
            case "java.lang.Double":
                return put((double)value);
            case "java.lang.String":
                return put((String) value);
            case "java.lang.Boolean":
                return put((boolean) value);
            case "org.bon.BON":
                return put((BON) value);
            case "org.bon.BONArray":
                return put((BONArray) value);
            default:throw new BONException("cant store object of type: "+value.getClass().getName());
        }
    }

    public final BONArray put(boolean value){
        array.add(value?BON.TRUE:BON.FALSE);
        return this;
    }

    public final BONArray put(String value){
        array.add(new BONString(value));
        return this;
    }

    public final BONArray put(BONArray value){
        array.add(value);
        return this;
    }

    public final BONArray put(BON value){
        array.add(value);
        return this;
    }

    public final BONArray put(long value){
        array.add(new BONInteger(value));
        return this;
    }

    public final BONArray put(int value){
        array.add(new BONInteger(value));
        return this;
    }
    public final BONArray put(float value){
        array.add(new BONFloat(value));
        return this;
    }

    public final BONArray put(double value){
        array.add(new BONDouble(value));
        return this;
    }
    @Override
    public final void write(DataOutputStream dos) throws IOException {
        ArrayList<BONValue> arr=array;
        int len=arr.size();
        if(len<=Byte.MAX_VALUE){
            dos.writeByte(BON_ARRAY8);
            dos.writeByte(len);
            writeValues(dos);
            return;
        }
        if(len<=Character.MAX_VALUE){
            dos.writeByte(BON_ARRAY16);
            dos.writeChar(len);
            writeValues(dos);
            return;
        }
        dos.writeByte(BON_ARRAY32);
        dos.writeInt(len);
        writeValues(dos);
    }

    private void writeValues(DataOutputStream dos) throws IOException {
        for(BONValue v:array)
            v.write(dos);
    }

    @Override
    public Object getValue() {
        return this;
    }

    @Override
    public String stringValue() {
        throw new ClassCastException();
    }

    @Override
    public byte[] byteArrayValue() {
        throw new ClassCastException();
    }

    @Override
    public String quote() {
        return toString();
    }

    public int getInt(int i) {
        return array.get(i).intValue();
    }
    public String getString(int i) {
        return array.get(i).stringValue();
    }

    public boolean getBoolean(int i) {
        return (boolean)array.get(i).getValue();
    }

    public BONArray getBONArray(int i) {
        return (BONArray) array.get(i);
    }

    public BON getBON(int i) {
       return (BON) array.get(i);
    }

    public long getLong(int i) {
        return array.get(i).longValue();
    }

    public float getFloat(int i) {
        return array.get(i).floatValue();
    }

    public double getDouble(int i) {
        return array.get(i).doubleValue();
    }


    public String toString(){
        StringBuilder sb=new StringBuilder();
        sb.append("[");
        String prefix="";
        for (BONValue value:array) {
            sb
                    .append(prefix)
                    .append(value.quote());
            prefix=",";
        }

        return sb.append("]").toString();
    }

    public final void write(OutputStream os){
        DataOutputStream dos=new DataOutputStream(os);
        try {
            write(dos);
        } catch (IOException e) {
            throw new BONException(e);
        }
    }

    public byte[] toByteArray(){
        ByteArrayOutputStream baos=new ByteArrayOutputStream();
        write(baos);
        return baos.toByteArray();
    }

    public Object get(int i) {
        return array.get(i).getValue();
    }
}
