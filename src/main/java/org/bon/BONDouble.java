package org.bon;
/*
    created by neoexpert at 28.01.21
*/

import java.io.DataOutputStream;
import java.io.IOException;

class BONDouble extends BONValue {
    private final double value;

    public BONDouble(double value) {
        this.value = value;
    }

    @Override
    public void write(DataOutputStream dos) throws IOException {
        dos.writeByte(BON.DOUBLE);
        dos.writeDouble(value);
    }

    @Override
    public final Object getValue() {
        return value;
    }

    @Override
    public final double doubleValue() {
        return value;
    }


    @Override
    public final String stringValue() {
        throw new ClassCastException("double cannot be converted to String");
    }

    @Override
    public byte[] byteArrayValue() {
        throw new ClassCastException("double cannot be converted to byte[]");
    }

    private String quoted;

    @Override
    public String quote() {
        if (quoted != null)
            return quoted;
        quoted = Double.toString(value);
        return quoted;
    }

    @Override
    public int intValue() {
        return (int) value;
    }

    @Override
    public long longValue() {
        return (long) value;
    }

    @Override
    public byte byteValue() {
        return (byte) value;
    }

    @Override
    public float floatValue() {
        return (float) value;
    }

    @Override
    public short shortValue() {
        return (short) value;
    }
}
