package org.bon;
/*
    created by neoexpert at 28.01.21
*/

public class EOFException extends BONException{
    public EOFException(){
			super("END of file");
		}
}
